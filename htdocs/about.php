<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <title>Assignment 02</title>
   <link rel="stylesheet" href="styles.css" />
</head>

<body style="background-image: url('images/stucco/stucco.png');">
<main>
  <div class="box">
    <div style= "background-color: black">
    <table  class="top">
      <tr>
        <th class="right"> My Account </th>
        <th class="right"> Wish List </th>
        <th class="right"> Shopping Cart </th>
        <th class="right"> Checkout </th>
      </tr>
    </table>
  </div>
  <h1> Art Store </h1>

  <div class="top2">
  <table style="width:40%">
    <tr>
      <th> <a href="index.php">Home</a> </th>
      <th> <a href="about.php">About Us</a> </th>
      <th> Art Works </th>
      <th> Artists </th>
    </tr>
  </table>
</div>

<h2> About us</h2>
<p>
  This assignment was created by Tyler Reinhart.
</p>

<p>
  It was created for CS3140 at Bowling Green University
</p>

<div style= "background-color: black">
<div class="bottom">
    All images are copyright to their owners. This is just a hypothetical site @ 2014 copyright art store
</div>
</div>

  </div>
</main>
</body>
