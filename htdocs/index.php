<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="utf-8">
   <title>Assignment 02</title>
   <link rel="stylesheet" href="styles.css" />
</head>

<body style="background-image: url('images/stucco/stucco.png');">


	<main>
    <div class="box">
      <div style= "background-color: black">
      <table  class="top">
        <tr>
          <th class="right"> <a href="#">Checkout</a> </th>
          <th class="right"> <a href="#">Shopping cart</a> </th>
          <th class="right"> <a href="#">Wish List</a> </th>
          <th class="right"> <a href="#">My Account</a> </th>
        </tr>
      </table>
    </div>
    <h1> Art Store </h1>
    <div class="top2">
    <table style="width:40%">
      <tr>
        <th> <a href="index.php">Home</a> </th>
        <th> <a href="about.php">About Us</a> </th>
        <th> Art Works </th>
        <th> Artists </th>
      </tr>
    </table>
  </div>

  <div class="d">
	<h2>Self-Portrait in a Straw Hat</h2>
  <h3> <a href="#">By Louise Elisabeth Lebrun</a> </h3>
      <div class="image">
         <a href="images/113010.jpg"><img src="images/113010.jpg" alt="113010.jpg" title="113010.jpg" height="378"/></a>
            <p>Lebrians original is recorded in a private collection in france</p>
            <h2>Similar Products</h2>

      </div>

<p>The Painting appears, after cleaning, to be an autograph replica of a
picture, the original of which was painted in Brussels in 1782 in free imitation of
Ruben's 'Chapeau de Paille'. which lebrun had seen in antwerp. it was
exhibitioned in Paris in 1782 at the Salon de la correspondance</p>

          <p class="red">$700</p>

          <button onclick="#"> Add to Wish List </button>
          <button onclick="#"> Add to Shopping Cart </button>

          <h2>Product Details</h2>

<table>
  <tr>
    <td>Date:</td>
    <td>1782</td>
  </tr>
  <tr>
    <td>Medium:</td>
    <td>Oil on canvas</td>
  </tr>
  <tr>
    <td>Dimenstions:</td>
    <td>98cm x 71cm</td>
  </tr>
  <tr>
    <td>Home:</td>
    <td><a href="https://en.wikipedia.org/wiki/Art">National Gallery London</a></td>
  </tr>
  <tr>
    <td>Genre:</td>
    <td><a href="https://en.wikipedia.org/wiki/Art">Realism, Rococo</a></td>
  </tr>
  <tr>
    <td>subjects:</td>
    <td><a href="https://en.wikipedia.org/wiki/Art">Aeople, Arts</a></td>
  </tr>
</table>


      </div>

      <div class="similar">

        <table>
          <tr>
            <th class="leeft"> <a href="images/thumbs/116010.jpg"><img src="images/thumbs/116010.jpg" alt="116010.jpg" title="116010.jpg" height="125"/></a> </th>
            <th class="leeft"> <a href="images/thumbs/120010.jpg"><img src="images/thumbs/120010.jpg" alt="120010.jpg" title="120010.jpg" height="125"/></a> </th>
            <th class="leeft"> <a href="images/thumbs/107010.jpg"><img src="images/thumbs/107010.jpg" alt="107010.jpg" title="107010.jpg" height="125"/></a> </th>
            <th class="leeft"> <a href="images/thumbs/106020.jpg"><img src="images/thumbs/106020.jpg" alt="106020.jpg" title="106020.jpg" height="125"/></a> </th>
          </tr>


          <tr>
            <td class="leeft"><a href="https://en.wikipedia.org/wiki/Art">Holding a Thistle</a></td>
            <td class="leeft"><a href="https://en.wikipedia.org/wiki/Art">Elanor of Toledo</a></td>
            <td class="leeft"><a href="https://en.wikipedia.org/wiki/Art">Madame de Pompadour</a></td>
            <td class="leeft"><a href="https://en.wikipedia.org/wiki/Art">Girl with a pearl earring</a></td>
          </tr>


        </table>





      </div>

      <div style= "background-color: black">
      <div class="bottom">
          All images are copyright to their owners. This is just a hypothetical site @ 2014 copyright art store
      </div>
      </div>
    </div>
	</main>

</body>


</html>
